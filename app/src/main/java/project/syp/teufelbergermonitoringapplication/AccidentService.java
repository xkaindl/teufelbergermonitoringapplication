package project.syp.teufelbergermonitoringapplication;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import com.google.gson.Gson;
import com.microsoft.signalr.HubConnection;
import com.microsoft.signalr.HubConnectionBuilder;

import static project.syp.teufelbergermonitoringapplication.MainActivity.alarmList;

public class AccidentService extends Service {
    private static final int MYSERVICE_ID = 111;
    NotificationManager manager;
    String errorMessage;
    private Thread worker;
    //Intent intentForPending = new Intent(this,MainActivity.class);
    //PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intentForPending, 0);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        worker = new Thread(this::doWork);
        worker.start();
        initHub();
    }

    public void initHub() throws InterruptedException {
        HubConnection hubConnection = HubConnectionBuilder
                .create("https://fp.teufelberger.com/MDE_Anlagenfehler/")
                .build();
        
        hubConnection.on("Receive", (message) -> receiveSignalRData(message));
        hubConnection.start().wait();
        fetchInitialFill(hubConnection);
    }

    private void receiveSignalRData(String message) {
        Gson gson = new Gson();
        String[] input = gson.fromJson(message, String[].class);
        if (input[0] == "Daten") {
            alarmList = gson.fromJson(input[1], Alarm.class);
        }

    }

    private void fetchInitialFill(HubConnection hubConnection) {
        String[] s = new String[]{"Force", ""};
        Gson gson = new Gson();
        String json = gson.toJson(s);
        hubConnection.invoke(String.class, "Transfer", json);
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private void doWork() {
        while (true) {
            try {
                if (true)//TODO:notification not existing
                {
                    Notification notification = generateNotification(errorMessage, 1);
                    manager.notify(MYSERVICE_ID, notification);
                }
            } catch (Exception e) {
                break;
            }
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private Notification generateNotification(String error, int importance) {
        long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500};
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Accident accident = new Accident(0, "", "", false, true);
        if (!accident.isHandled) alarmList.add(accident);
        Notification notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.ic_menu_send)
                .setContentTitle("Anlage 13")
                .setContentText("Alles kapput rip")
                .setVibrate(pattern)
                .setSound(alarmSound)
                .setOnlyAlertOnce(false)
                //.setContentIntent(pendingIntent)
                .build();

        return notification;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        errorMessage = intent.getStringExtra("errorMessage");
        return super.onStartCommand(intent, flags, startId);
    }
}
