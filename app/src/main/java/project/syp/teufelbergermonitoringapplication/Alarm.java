package project.syp.teufelbergermonitoringapplication;

import android.graphics.Color;

import java.util.Date;

public class Alarm {
    public boolean anzeige;
    public boolean isBlinking;
    public String text;
    public boolean isActive;
    public Date timestamp;
    public Color foreColor;
    public int index;
    public Color backColor;
}
