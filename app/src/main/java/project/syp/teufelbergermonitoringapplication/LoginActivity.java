package project.syp.teufelbergermonitoringapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;

public class LoginActivity extends AppCompatActivity {
    protected EditText user;
    protected EditText pass;
    public static String userName;

    private Button btnLogin;
    private final String url = "https://fp.teufelberger.com/Error_App/Handler.ashx";
    private final String AppIdentifier = "ErrorApp";
    private final String TAG = "LoginAct";
    private final Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity_layout);
        initViews();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
    //region alternativeLogin !not working!
    /*
    public void login(String username, String password) throws IOException {

        String urlParameters = "ID=" + AppIdentifier + "&User=" + username + "&Password=" + password;
        HttpURLConnection con = initConnection();
        sendLoginData(urlParameters, con);
        MainActivity.loginIdentifier = fetchLoginResponseParams(urlParameters, con);
    }

    private String fetchLoginResponseParams(String urlParameters, HttpURLConnection con) throws IOException {
        int responseCode = con.getResponseCode();

        Log.d(TAG, "\nSending 'POST' request to URL : " + url);
        Log.d(TAG, "Post parameters : " + urlParameters);
        Log.d(TAG, "Response Code : " + responseCode);

        if (responseCode != HttpURLConnection.HTTP_OK) return null;
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        Log.d(TAG, response.toString());
        return response.toString();
    }

    private void sendLoginData(String urlParameters, HttpURLConnection con) throws IOException {
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();
    }

    @NonNull
    private HttpURLConnection initConnection() throws IOException {
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("POST");//
        con.setDoOutput(true);
        return con;
    }
    */
    //endregion
    private void login(String userName, String password) {
        Log.d(TAG, "login");
        new Thread(() -> {
            try {
                byte[] data = ("ID=" + AppIdentifier + "&User=" + userName + "&Password=" + password).getBytes();
                HttpURLConnection connection = getPostConnection(data, url);
                int responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String sData = responseToString(connection.getInputStream()); //connection.getResponseMessage()
                    Log.d(TAG, "Login identifier received: " + sData);
                    runOnUiThread(() -> MainActivity.loginIdentifier = sData);
                }
                runOnUiThread(() -> {
                    Intent intent = new Intent(context, MainActivity.class);
                    startActivity(intent);
                });
            } catch (IOException e) {
                e.printStackTrace();
            }

        }).start();
    }

    private String responseToString(InputStream bin) throws IOException {
        Log.d(TAG, "responseToString");
        final int bufferSize = 1024;
        final char[] buffer = new char[bufferSize];
        final StringBuilder sb = new StringBuilder();
        Reader reader = new InputStreamReader(bin, "UTF-8");
        while (true) {
            int nrCharsRead = reader.read(buffer, 0, buffer.length);
            if (nrCharsRead < 0) break;
            sb.append(buffer, 0, nrCharsRead);
        }
        return sb.toString();

    }

    private HttpURLConnection getPostConnection(byte[] data, String url) throws IOException {
        Log.d(TAG, "build connection");
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
        connection.setFixedLengthStreamingMode(data.length);
        connection.getOutputStream().write(data);
        connection.getOutputStream().flush();
        return connection;
    }

    private void initViews() {
        user = findViewById(R.id.user);
        pass = findViewById(R.id.pass);
        btnLogin = findViewById(R.id.btnLogin);
    }

    public void onClickLogin(View view) throws IOException {
        Log.d(TAG, "login trigger");
        userName = user.getText().toString();
        login(user.getText().toString(), pass.getText().toString());
    }
}
